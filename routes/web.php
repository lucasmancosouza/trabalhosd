<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
**    return view('welcome');
**  });
*/

Route::get('/', ['as'=>'home','uses'=> 'Controller@home']);
Route::Any('store',['as'=>'store','uses'=> 'Controller@store']);
