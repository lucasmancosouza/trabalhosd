<?php


namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Flash;
use Illuminate\Support\MessageBag;
use Khill\Lavacharts\Lavacharts;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function store(Request $request){
            $validator = Validator::make($request->all(), [
                "name"  => "string",
                "email" => ["email",'unique:users,email'],
                "exe_g" => "numeric",
                "com_g" => "numeric",
                "pla_g" => "numeric",
                "ana_g" => "numeric",
            ]);

            if ($validator->fails()) {
               return view('welcome')
                         ->withErrors($validator);
            }
            $input=[
              "name"  => $request->get('name'),
              "email" => $request->get('email'),
              "exe_g" => $request->get('exe_g'),
              "com_g" => $request->get('com_g'),
              "pla_g" => $request->get('pla_g'),
              "ana_g" => $request->get('ana_g'),
            ];
            DB::table('users')->insert($input);
            Flash::success('Salvo com sucesso.');
            return view('welcome');
    }
    public function home(){
         $users = DB::table('users')->get();
         $exe_g=0;
         $com_g=0;
         $pla_g=0;
         $ana_g=0;
         $count=0;
         foreach ($users as $user) {
             $exe_g+=$user->exe_g;
             $com_g+=$user->com_g;
             $pla_g+=$user->pla_g;
             $ana_g+=$user->ana_g;
             $count++;
         }
         $exe_g=$exe_g/$count;
         $com_g=$com_g/$count;
         $pla_g=$pla_g/$count;
         $ana_g=$ana_g/$count;

         /*
        **
        **  Grafico Perfil DISC
        **
        */
        $lava = new Lavacharts;

        $stocksTable = $lava->DataTable();
        $stocksTable  ->addStringColumn('Perfil')
                      ->addNumberColumn('valor geral');
        $stocksTable->addRow(['Executor', $exe_g]);
        $stocksTable->addRow(['Comunicador', $com_g]);
        $stocksTable->addRow(['Planejador', $pla_g]);
        $stocksTable->addRow(['Analista', $ana_g]);

        /*$stocksTable->addRow(['Executor', 35.73]);
        $stocksTable->addRow(['Comunicador', 38.28]);
        $stocksTable->addRow(['Planejador', 16.24]);
        $stocksTable->addRow(['Analista', 9.74]);*/
        $areachart = $lava->PieChart( "Perfil DISC", $stocksTable,[
            'colors'=>['#ff0000','#ffff00','#00cc00','#3366ff'],
            'is3D' => true,
            'pieSliceTextStyle' => ['color'=>'#000000']


        ]);
         return view('welcome')
                    ->with('users',$users)
                    ->with('lava', $lava);
    }
}
