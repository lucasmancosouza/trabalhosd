
<!-- Nome Field -->
<div class="">
    {!! Form::label('name', 'Nome:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>


<!--===========================================================================-->
<!-- Executor Field -->
<div class="">
    <div class="input-group-addon">
      {!! Form::label('resultado', 'Executor:') !!}
      {!! Form::text('exe_g',null, ['class' => 'form-control']) !!}
    </div>
</div>

<!--===========================================================================-->
<!-- Comunicador Field -->
<div class="">
    <div class="input-group-addon">
      {!! Form::label('resultado', 'Comunicador:') !!}
      {!! Form::text('com_g',null, ['class' => 'form-control']) !!}
    </div>
</div>

<!--===========================================================================-->
<!-- Planejador Field -->
<div class="">
    <div class="input-group-addon">
      {!! Form::label('resultado', 'Planejador:') !!}
      {!! Form::text('pla_g',null, ['class' => 'form-control']) !!}
    </div>
</div>


<!--===========================================================================-->
<!-- Analista Field -->
<div class="">
    <div class="input-group-addon">
      {!! Form::label('resultado', 'Analista:') !!}
      {!! Form::text('ana_g',null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
